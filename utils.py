import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.preprocessing import StandardScaler
from sklearn.utils.validation import check_is_fitted, _deprecate_positional_args
import matplotlib.pyplot as plt


def sim(model_results, size=10):
    """
    Simulate samples from coefficients of a linear model (analogous to R function sim() from Gelman and Hill).
    The standard deviation of residuals $\hat{\sigma}$ is chi-squared while the model coefficients
    $\hat{beta}$ are normal.
    Both uncertainty in $\hat{\sigma}$ and $\hat{beta}$ is taken into account.
    PARAMETERS:
        model_results : the results object returned from `fit()` method of a statsmodels linear model class
        size : int the number of simulations to run
    RETURN:
        beta_sim : np.array of shape [size, len(model_results.params)]
    """
    beta_hat = model_results.params
    cov_beta = model_results.cov_params()
    df = model_results.df_resid
    sigma_hat = np.sqrt(np.sum(model_results.resid**2) / df)
    V_beta = cov_beta / sigma_hat**2
    X = np.random.chisquare(df, size=size)
    sigma_sim = sigma_hat * np.sqrt(df / X)
    beta_sim = [np.random.multivariate_normal(beta_hat, (sigma_sim[i]**2) * V_beta) for i in range(size)]
    return np.array(beta_sim)

def standardize(data, center_ref=None, scale_factor=2):
    """
    Standardize variables in `data`.
    PARAMETERS: 
        data : np.array or pandas DataFrame with samples on the rows and variable on the columns
        center_ref : 
    """
    if center_ref is None:
        center_ref = data.mean()
    if np.isscalar(scale_factor):
        scale_factor = scale_factor * data.std()        
    #assert (len(scale_factor)==np.shape(data)[1]), "len(scale_factor) has to be equal to the number of columns in data!"
    #assert (len(center_ref)==np.shape(data)[1]), "len(center_ref) has to be equal to the number of columns in data!"
    return (data - center_ref) / scale_factor

def plot_model_coefs(model, ax=None, include_const=False):
    if ax is None:
        fig, ax = plt.subplots()
    if not include_const:
        ax.errorbar(model.params.keys()[1:], model.params[1:], model.bse[1:]*2, fmt='none')
        ax.scatter(model.params.keys()[1:], model.params[1:])
    else:
        ax.errorbar(model.params.keys(), model.params, model.bse*2, fmt='none')    
        ax.scatter(model.params.keys(), model.params)
    return ax

def jitter_binary(a, jitter=0.05):
    """
    Jitter a binary variable.
    """
    jittered = np.random.rand(len(a)) * jitter
    jittered[a==1] += (1-jitter)
    #np.array([np.random.rand()*jitter if i==0 else (np.random.rand()*jitter + (1-jitter)) for i in a ])
    return jittered

def jitter(a, jitter=0.05):
    """
    Jitter a categorical variable.
    """
    levels = np.unique(a)
    n_levels = len(levels)
    jittered = np.random.rand(len(a)) * jitter
    for l in levels:
        jittered[a==l] += (l-jitter/2)
    return jittered


class SMWrapper(BaseEstimator, ClassifierMixin):
    """ Sklearn-style wrapper for statsmodels GLM regressors with categorical endogeous variable """
    def __init__(self, model_class, fit_intercept=True):
        self.model_class = model_class
        self.fit_intercept = fit_intercept
    def fit(self, X, y):
        if self.fit_intercept:
            X = sm.add_constant(X)
        self.model_ = self.model_class(y, X)
        self.results_ = self.model_.fit()
    def predict_proba(self, X):
        # WEAK check: only checks for a column named 'const' if the intercept has a different name might be wrong
        if self.fit_intercept and 'const' not in X:
            # the constant has to be added even if there's already a column with constant value
            # this could happen with test data where not all variation is present
            X = sm.add_constant(X, has_constant='add')
        return self.results_.predict(X)
    def predict(self, X):
        # WEAK check: only checks for a column named 'const' if the intercept has a different name might be wrong
        if self.fit_intercept and 'const' not in X:
            # the constant has to be added even if there's already a column with constant value
            # this could happen with test data where not all variation is present
            X = sm.add_constant(X, has_constant='add')
        y_pred_prob = self.results_.predict(X)
        return (y_pred_prob>0.5).astype(int)
    
class AddInteractions(BaseEstimator, TransformerMixin):
    def __init__(self, interactions):
        self.interactions = interactions
     
    def fit(self, X, y=None):
        pass
        return self
 
    def transform(self, X):
        # make sure that it was fitted
        #check_is_fitted(self, '_interactions_dict')
        X = X.copy()
        for i in self.interactions:
            X[i[0]+':'+i[1]] = X[i[0]] * X[i[1]]
        return X
    
class PartialStandardScaler(StandardScaler):
    """Scaler that allows to only standardize some columns. This might be useful with binary variables
    for interpretability of coefficients.
        NOTE: only works with pandas DataFrames.
    Parameters
    ----------
    copy : boolean, optional, default True
        If False, try to avoid a copy and do inplace scaling instead.
        This is not guaranteed to always work inplace; e.g. if the data is
        not a NumPy array or scipy.sparse CSR matrix, a copy may still be
        returned.
    with_mean : boolean, True by default
        If True, center the data before scaling.
        This does not work (and will raise an exception) when attempted on
        sparse matrices, because centering them entails building a dense
        matrix which in common use cases is likely to be too large to fit in
        memory.
    with_std : boolean, True by default
        If True, scale the data to unit variance (or equivalently,
        unit standard deviation).
    scale_factor : integer, 1 by default.
        An integer that multiplies the standard deviation of each column. This might be useful
        for interpretability with binary variable where it is adviced to standardize by 2 standard
        deviations (Gelman, Hill, 2004).
    all_but : {array-like}
        List of strings with the names of columns to leave unscaled. 

    """
    @_deprecate_positional_args
    def __init__(self, *, copy=True, with_mean=True, with_std=True, scale_factor=1, all_but=None):
        self.with_mean = with_mean
        self.with_std = with_std
        self.scale_factor = scale_factor
        self.copy = copy
        self.all_but = all_but
    
    def fit(self, X, y=None):
        """Compute the mean and std to be used for later scaling. Allows to only standardize some columns.
        NOTE: only works with pandas DataFrames.
        Parameters
        ----------
        X : {array-like, sparse matrix}, shape [n_samples, n_features]
            The data used to compute the mean and standard deviation
            used for later scaling along the features axis.
        y
            Ignored 
        """
        # Reset internal state before fitting
        #self._reset()
        self.mean_ = X.mean()
        self.scale_ = X.std() * self.scale_factor
        if self.all_but is not None:
            for column in self.all_but:
                self.mean_[column] = 0
                self.scale_[column] = 1
        return self
    
    def transform(self, X):
        if self.with_mean:
            X -= self.mean_
        if self.with_std:
            X /= self.scale_
        return X
